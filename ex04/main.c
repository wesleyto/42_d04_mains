#include <stdio.h>
#include <time.h>
#include "ft_fibonacci.c"
int		main(void)
{	
	int cases[] = {-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 40};
	int exp_res[] = {-1, -1, 0, 1, 1, 2, 3, 5, 8, 13, 102334155};
	clock_t t;
	int result;
	char *result_str;

	for (int i = 0; i < 11; i++)
	{
		int test_case = cases[i];
		int exp_result = exp_res[i];
		t = clock();
		result = ft_fibonacci(test_case);
		t = clock() - t;
		result_str = exp_result == result ? "Success!" : "Failure!";
		double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
		printf("Case: %-3d || %s || Exp: %-10d || Got: %-10d || Time: %f secs\n", test_case, result_str, exp_result, result, time_taken);
	}
}