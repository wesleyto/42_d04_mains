#include <stdio.h>
#include <time.h>
#include "ft_recursive_factorial.c"
int		main(void)
{	
	int cases[] = {0, 1, -1, 12, 13, 5, 2};
	int exp_res[] = {1, 1, 0, 479001600, 0, 120, 2};
	clock_t t;
	int result;
	char *result_str;

	for (int i = 0; i < 7; i++)
	{
		int test_case = cases[i];
		int exp_result = exp_res[i];
		t = clock();
		result = ft_recursive_factorial(test_case);
		t = clock() - t;
		result_str = exp_result == result ? "Success!" : "Failure!";
		double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
		printf("Case: %-3d || %s || Exp: %-10d || Got: %-10d || Time: %f secs\n", test_case, result_str, exp_result, result, time_taken);
	}
}