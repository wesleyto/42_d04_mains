#include <stdio.h>
#include <time.h>
#include "ft_sqrt.c"
int		main(void)
{	
	int cases[] = {-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 2147395600};
	int exp_res[] = {0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 0, 3, 46340};
	clock_t t;
	int result;
	char *result_str;

	for (int i = 0; i < 13; i++)
	{
		int test_case = cases[i];
		int exp_result = exp_res[i];
		t = clock();
		result = ft_sqrt(test_case);
		t = clock() - t;
		result_str = exp_result == result ? "Success!" : "Failure!";
		double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
		printf("Case: %-3d || %s || Exp: %-10d || Got: %-10d || Time: %f secs\n", test_case, result_str, exp_result, result, time_taken);
	}
}