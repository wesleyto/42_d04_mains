#include <stdio.h>
#include <time.h>
#include "ft_iterative_power.c"
int		main(void)
{	
	int cases1[] = {0, 2, 2, 2, 2};
	int cases2[] = {1, 3, 10, -1, 0};
	int exp_res[] = {0, 8, 1024, 0, 1};
	clock_t t;
	int result;
	char *result_str;

	for (int i = 0; i < 5; i++)
	{
		int test_case = cases1[i];
		int test_case2 = cases2[i];
		int exp_result = exp_res[i];
		t = clock();
		result = ft_iterative_power(test_case, test_case2);
		t = clock() - t;
		result_str = exp_result == result ? "Success!" : "Failure!";
		double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
		printf("Case: %-3d, %-3d || %s || Exp: %-10d || Got: %-10d || Time: %f secs\n", test_case, test_case2, result_str, exp_result, result, time_taken);
	}
}