# README #

### What is this repository for? ###

* 42 d04 evaluations

### Notes ###

* ex08 and ex09 missing. I will not be uploading them.
* Feel free to template the other main.c's for ex08 and ex09.

### How do I get set up? ###

* Run the included script to check norm, compile, and run the test cases.
* You should still examine code to check that the other instructions were followed.
* NO GUARANTEES. I have found and corrected mistakes in these test files myself, and there may be more, or there may be missing test cases.
* Always examine the test cases and the results, and interpret them for yourself.
* This speeds up the evaluation process, but does not replace it. Don't rely on it.

### Who do I talk to? ###

* Repo owner